class ChangePriceColumnService < ActiveRecord::Migration
  def change
  	change_column :services, :price, :decimal, default: 0.00, precision: 8, scale: 2
  end
end
