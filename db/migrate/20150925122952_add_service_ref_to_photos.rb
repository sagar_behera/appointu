class AddServiceRefToPhotos < ActiveRecord::Migration
  def change
  	add_reference :photos, :service, index: true, foreign_key: true
  end
end
