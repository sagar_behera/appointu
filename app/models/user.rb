class User < ActiveRecord::Base
	before_save :downcase_email

	#Associations
	has_many :services, dependent: :destroy
	has_many :appointments

	#Validations
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :name, presence: true, length: {maximum: 255}
	validates :email, presence: true, length: {maximum: 255},format: {with: VALID_EMAIL_REGEX },
				uniqueness: {case_sensitive: false}
	has_secure_password
	validates :password, presence: true, length: {minimum: 5}
	validates :user_type, presence: true

	private
	def downcase_email
		self.email = email.downcase
	end
end
