class Photo < ActiveRecord::Base
	belongs_to :service

	#Paperclip setup
	has_attached_file :image,
		styles: {thumbnail: "100x100>", medium: "300x300>", large: "600x600>"},
		default_url: "/images/:style/missing.png"
	validates_attachment :image, content_type: {content_type: ["image/jpg", "image/png", "image/jpeg", "image/gif"]}

end
