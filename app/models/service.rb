class Service < ActiveRecord::Base
	#Associations
	belongs_to :user
	has_many :photos, dependent: :destroy
	has_many :appointments

	#Validations
	validates :title, presence: true, length: {maximum: 255}
	
end
