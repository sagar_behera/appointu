class Appointment < ActiveRecord::Base
  belongs_to :user
  belongs_to :service

 def check_doctor_schedule
 	doctor_schedules = Appointment.where(doctor_id: self.doctor_id).where(status: "confirmed").where("start_time BETWEEN ? AND ?", self.start_time.beginning_of_day, self.start_time.end_of_day)
  		if doctor_schedules
	  		doctor_schedules.each do |schedule|
			 	if (self.start_time >= schedule.start_time) && (self.start_time <= (schedule.start_time + (schedule.service.duration/60).hours))
			  		return true
				end 
	  		end
	  		return false
  		end
 end
end
