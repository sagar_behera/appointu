class ServicesController < ApplicationController
	respond_to :js, :html

	def create
		@service = Service.new(service_params)

		respond_to do |format|
			if @service.save
			
				if !params[:image].blank?
					params[:image].each do |img|
						@service.photos.create(image: img)
					end
				end
				format.js {render 'create'}
			else
				format.js {render js: "Failed to create service" }
			end #if @service.save
		end #respond_to
	end #Create

	private

	def service_params
		params.require(:service).permit(:title,:duration,:price,:desc,:user_id)
	end
end
